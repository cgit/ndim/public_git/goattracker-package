#!/bin/sh

set -ex

version=$1
if test "x$version" = "x"; then
    echo "Usage: $(basename "$0") VERSION"
    exit 13
fi
baseurl=http://prdownloads.sourceforge.net/goattracker2

function cleanup_zipfile() {
    zip_suffix=$1
    tar_suffix=$2
    zipfile=GoatTracker_${version}${zip_suffix}.zip
    url=${baseurl}/${zipfile}

    test -f "$zipfile" || wget "$url"

    subdir=goattracker-${version}${tar_suffix}
    rm -rf "$subdir/"

    unzip -d "$subdir" "$zipfile"

    find "$subdir" \( -name '*.exe' -or -name '*.dll' \) -print -exec rm -f {} \;
    rmdir "$subdir"/* ||:

    tar cvf - "${subdir}/" | xz -c > "${subdir}.tar.xz"
}

cleanup_zipfile
# cleanup_zipfile _Stereo -stereo
