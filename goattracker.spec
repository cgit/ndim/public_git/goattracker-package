Name:		goattracker
Version:	2.68
Release:	0.1%{?dist}
Summary:	Music editor for creating Commodore 64 music

Group:		Applications/Multimedia
License:	GPLv2+
URL:		http://sourceforge.net/projects/goattracker2/
# Original source location:
#  * http://prdownloads.sourceforge.net/goattracker2/GoatTracker_%{version}.zip
#  * http://prdownloads.sourceforge.net/goattracker2/GoatTracker_%{version}_Stereo.zip
# These zipfiles need to be cleaned of executables, and as that means we need
# to repackage them anyway, we can use .tar.xz for that.
# Run "./cleanup-sources.sh 2.68" to download upstream sources (if necessary)
# and cleaning them up.
Source0:	%{name}-%{version}.tar.xz
# We need our own make files. The existing ones strip binaries, drop cflags, etc
Source10:	Makefile-bme
Source11:	Makefile-src
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	SDL-devel


%description
GoatTracker is a crossplatform music editor for creating Commodore 64
music. Uses reSID library by Dag Lem and supports alternatively also
HardSID & CatWeasel devices. Version 2.68 adds Antti Lankila's
improved reSID distortion code.


%prep
%setup -q
cp %{SOURCE10} Makefile-bme
cp %{SOURCE11} Makefile-src
sed -i \
    -e '/^	strip $@/d' \
    -e '/^CC=/d' \
    -e '/^CXX=/d' \
    -e's/^	\(datafile\|dat2inc\) /	.\/bme\/\1 /' \
    src/makefile.common
for f in authors readme.txt readme_resid.txt readme_sdl.txt
do
	iconv -f WINDOWS-1252 -t UTF8 < "$f" > "$f.new"
	mv -f "$f.new" "$f"
	sed -i 's/$//' "$f"
done


%build
make CFLAGS="%{optflags}" -f "$PWD/Makefile-bme" -C src/bme %{?_smp_mflags}
make \
     CFLAGS="%{optflags} -Wall -Ibme -Iasm" \
     SDL_CFLAGS="$(sdl-config --cflags)" \
     SDL_LIBS="$(sdl-config --libs)" \
     -f "$PWD/Makefile-src" -C src %{?_smp_mflags}


%install
rm -rf "%{buildroot}"
make -f "$PWD/Makefile-src" -C src \
     install DESTDIR="%{buildroot}" BINDIR="%{_bindir}"


%clean
rm -rf "%{buildroot}"


%files
%defattr(-,root,root,-)
%doc authors copying readme.txt
%doc readme_sdl.txt readme_resid.txt
%doc goat_tracker_commands.pdf
%{_bindir}/betaconv
%{_bindir}/goattrk2
%{_bindir}/ins2snd2
%{_bindir}/mod2sng
%{_bindir}/sngspli2


%changelog
* Sun Dec  6 2009 Hans Ulrich Niedermann <hun@n-dimensional.de> - 2.68-0.1
- Initial rpmlint-clean package

